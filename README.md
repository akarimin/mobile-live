## Retail Service assignment - Mobile-LIVE code challenge

#### To Run:

To run the project please execute the command below:

<code>./mvnw spring-boot:run</code>

As soon as the project is up and running please visit:

<url>http://localhost:8181/swagger-ui.html</url>

I would be more than happy to answer any question you might come across
so please do not hesitate to shoot me an email at:

<email>akarimin@buffalo.edu</email>


#### What is Done ?

The application is developed by a backend developer (me :D) so I have
taken advantage of Swagger2-UI to visually represent my api.
Different aspects of Core Java, Spring Framework, OOP and Design Pattern
have been applied in the project, including:

## Spring Boot
To boot the project, I have used Spring Boot served by embedded Tomcat.
Due to the due date limitation, I have not customized embedded Jetty
which has advantages over Tomcat in web environment. So In case of
production stage, I usually set up a Jetty application server to boot.

## Spring IoC
Dependancy injection has been widely used in the project, in particular,
Inversion of Control by constructor injection because of providing
possibility of injection in test environment.

## Spring MVC
The application is an example of MVC architecture with Swagger-UI as
the view layer some-how. Layers have been segregated as ControllerAdvice,
RestController, Service, Repository.

## Data Transfer Object Design Pattern
DTO design pattern has been implemented as a medium of conveying data
from Controller layer to Service layer and vice versa. So on service
layer, a mapper library (Mapstruct) has been used in order to map the
business objects (DTO) into Entity objects and the way around.

## Loose-Coupling, High-Cohesion
In architecture and design of the project I have tried my best to design
the layers, services, and even the entities decoupled, so if any changes
requested by the client/employer each component could be replaced with
another with low refactoring effort, budget and time.

## Builder Design Pattern
Instantiation of the objects are implemented based on builder pattern
in order to lessen the human errors and possible side effects while
mutating the fields as well as keeping the Functional Programming code
style.

## Spring Data-JPA
In DAO layer, I have taken Spring Data for granted to transact with
In-Memory Apache Derby database. My favorite ORM, Hibernate, with
the Java Persistence api have been wrapped by Spring Data to make the
lives of developers much easier.

## Spring AOP
Aspect Oriented Programming is an exceptional feature of Spring which I
have been exploited in RestController layer in order to validate request
beans. By setting up an Aspect on this layer, all invalidated fields
would be identified and the corresponding field error would be returned
to the client.
#### WARNING:
Because of using Spring-Fox Swagger and considering this stage of the
project as DEVELOPMENT, JSR303 bean validations such as javax.validation
and hibernate.validator have conflicts with Swagger and it is considered
as temporary. As soon as moving the application to integration phase
with clients (front-end, mobile apps), Swagger would be disabled and bean
validations come into play. So No worries for now.

## Spring Java Format (Code Style)
In this project, Spring-JavaFormat code style has been globally applied
by the command below, so doing the same in a team of developers prevents
code style conflicts.

<code>./mvnw spring-javaformat:apply</code>

## Spring Fox
Swagger2 Api has been configured to provide an out-of-box user interface
as well as a client-developer friendly and readable api.

## Docker
To deploy in a Dockerized environment please execute the command below:

<code>./mvnw install dockerfile:build</code>

and set DOCKER_HOST as your public ip so the application will be listened
on port 2376 by default.

## Spring Actuator
In order to monitor vital key indicators of the api, a few Actuator api
endpoints have been enabled including health, info and shutdown.

## Spring Profile
DEV and PROD profiles have been configured in pom.xml enabling us to
differentiate the build process on either environments. For example to
integrate with front-end on PROD we could install node and npm
from scratch while this step could be cumbersome for front-end developers
during their DEV so profiling makes their life easier in a way that they
can build the front-layer standalone on node and proxy the port to the
port that a stable version of backend is listened.

## LICENSE
The project is under GNU Lesser General Public License
please visit <http://www.gnu.org/licenses/>