/*
 * Copyright (c) 2018 MobileLIVE corporation.
 *    This file is part of retail-service.
 *    retail-service is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *    retail-service is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with retail-service.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mobilelive.retail.util;

import org.reflections.Reflections;

import java.io.*;
import java.util.Arrays;
import java.util.Properties;

import static ca.mobilelive.retail.constant.Constants.BASE_PACKAGE;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Objects.isNull;

/**
 * @author akarimin@buffalo.edu
 */

public class Props {

	private final static Properties PROP = new Properties();

	static {
		try {
			loadMessages();
		}
		catch (Exception e) {
			e.printStackTrace();
			System.exit(2);
		}
	}

	@SuppressWarnings("ConstantConditions")
	private static void loadMessages() {
		Reflections reflections = new Reflections(BASE_PACKAGE);
		reflections.getTypesAnnotatedWith(MessageProvider.class).stream()
				.map(c -> c.getAnnotation(MessageProvider.class))
				.map(MessageProvider::value).flatMap(Arrays::stream)
				.map(name -> name + ".properties")
				.map(Props.class.getClassLoader()::getResourceAsStream)
				.map(stream -> new BufferedReader(new InputStreamReader(stream, UTF_8)))
				.forEach(Props::fillProps);
	}

	private static void fillProps(Reader in) {
		try {
			PROP.load(in);
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public static void fillProps(String fileName) {
		try {
			PROP.load(new InputStreamReader(new FileInputStream(fileName), UTF_8));
		}
		catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public static String property(String key, Object... params) {
		String message = PROP.getProperty(key);
		if (isNull(message))
			message = key;
		for (int i = 0; params != null && i < params.length; i += 2)
			message = message.replace("{" + params[i] + "}",
					String.valueOf(params[i + 1]));
		return message;
	}

	public static Properties getProp() {
		return PROP;
	}

}
