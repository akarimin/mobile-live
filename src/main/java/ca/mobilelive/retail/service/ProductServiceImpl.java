/*
 * Copyright (c) 2018 MobileLIVE corporation.
 *    This file is part of retail-service.
 *    retail-service is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *    retail-service is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with retail-service.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mobilelive.retail.service;

import ca.mobilelive.retail.domain.Category;
import ca.mobilelive.retail.domain.Product;
import ca.mobilelive.retail.dto.request.ProductDTO;
import ca.mobilelive.retail.dto.response.GetProductListResponse;
import ca.mobilelive.retail.exception.message.ProductNotFoundException;
import ca.mobilelive.retail.mapper.ProductMapper;
import ca.mobilelive.retail.repository.ProductRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.util.Date;

import static io.micrometer.core.instrument.util.StringUtils.isNotEmpty;
import static java.util.Objects.nonNull;
import static java.util.stream.Collectors.toList;
import static org.springframework.data.domain.PageRequest.of;

/**
 * @author akarimin@buffalo.edu
 */

@Service
public class ProductServiceImpl implements ProductService<ProductDTO> {

	private final static Logger LOGGER = LoggerFactory
			.getLogger(ProductServiceImpl.class);

	private final ProductMapper productMapper;

	private final ProductRepository productRepository;

	@Autowired
	public ProductServiceImpl(ProductRepository productRepository,
			ProductMapper productMapper) {
		this.productRepository = productRepository;
		this.productMapper = productMapper;
	}

	@Override
	public ProductDTO createProduct(ProductDTO request) {
		return productMapper
				.toDto(productRepository.saveAndFlush(productMapper.toEntity(request)));
	}

	@Override
	public GetProductListResponse retrieveAllProduct(int page, int size)
			throws ProductNotFoundException {
		Page<Product> productList = productRepository.findAll(of(page, size));
		if (page > productList.getTotalPages())
			throw new ProductNotFoundException();
		return new GetProductListResponse()
				.setProducts(productList.getContent().stream().map(productMapper::toDto)
						.collect(toList()))
				.setTotalRecord(productList.getTotalElements());
	}

	@Override
	public GetProductListResponse retrieveAllProductByCategory(String category, int page,
			int size) throws ProductNotFoundException {
		Page<Product> productList = productRepository
				.findAllProductsByCategoryPaginated(category, of(page, size));
		if (page > productList.getTotalPages())
			throw new ProductNotFoundException();
		return new GetProductListResponse()
				.setProducts(productList.getContent().stream().map(productMapper::toDto)
						.collect(toList()))
				.setTotalRecord(productList.getTotalElements());
	}

	@Override
	@Transactional
	public ProductDTO retrieveProductById(long productId)
			throws ProductNotFoundException {
		try {
			Product product = productRepository.getOne(productId);
			return productMapper.toDto(product);
		}
		catch (EntityNotFoundException e) {
			LOGGER.error("Product not found with id : {}", productId);
		}
		throw new ProductNotFoundException(productId);
	}

	@Override
	@Transactional
	public ProductDTO updateProduct(ProductDTO request) throws ProductNotFoundException {
		try {
			Product product = productRepository.getOne(request.getProductId());
			product.setCategory(isNotEmpty(request.getCategory()) ? new Category()
					.setLastUpdate(new Date()).setName(request.getCategory())
					: product.getCategory())
					.setCode(isNotEmpty(request.getCode()) ? request.getCode()
							: product.getCode())
					.setDescription(isNotEmpty(request.getDescription())
							? request.getDescription() : product.getDescription())
					.setLastUpdate(new Date())
					.setPrice(nonNull(request.getPrice()) ? request.getPrice()
							: product.getPrice())
					.setQuantity(nonNull(request.getQuantity()) ? request.getQuantity()
							: product.getQuantity());
			productRepository.save(product);
			return productMapper.toDto(productRepository.getOne(request.getProductId()));
		}
		catch (EntityNotFoundException e) {
			LOGGER.error("Product not found with id : {}", request.getProductId());
		}
		throw new ProductNotFoundException(request.getProductId());
	}

	@Override
	public void deleteProductById(long productId) throws ProductNotFoundException {
		try {
			Product product = productRepository.getOne(productId);
			if (nonNull(product))
				productRepository.deleteById(productId);
		}
		catch (Exception e) {
			LOGGER.error("Product not found with id : {}", productId);
			throw new ProductNotFoundException(productId);
		}
	}

}
