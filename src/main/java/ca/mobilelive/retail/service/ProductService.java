/*
 * Copyright (c) 2018 MobileLIVE corporation.
 *    This file is part of retail-service.
 *    retail-service is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *    retail-service is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with retail-service.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mobilelive.retail.service;

import ca.mobilelive.retail.dto.response.GetProductListResponse;
import ca.mobilelive.retail.exception.message.ProductNotFoundException;

/**
 * @author akarimin@buffalo.edu
 */

public interface ProductService<T> {

	T createProduct(T product);

	T retrieveProductById(long productId) throws ProductNotFoundException;

	GetProductListResponse retrieveAllProduct(int page, int size)
			throws ProductNotFoundException;

	GetProductListResponse retrieveAllProductByCategory(String category, int page,
			int size) throws ProductNotFoundException;

	T updateProduct(T product) throws ProductNotFoundException;

	void deleteProductById(long productId) throws ProductNotFoundException;

}
