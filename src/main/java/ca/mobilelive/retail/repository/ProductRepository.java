/*
 * Copyright (c) 2018 MobileLIVE corporation.
 *    This file is part of retail-service.
 *    retail-service is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *    retail-service is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with retail-service.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mobilelive.retail.repository;

import ca.mobilelive.retail.domain.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * @author akarimin@buffalo.edu
 */

public interface ProductRepository extends JpaRepository<Product, Long> {

	@Query(value = "SELECT * FROM APP_RETAIL.ml_product p INNER JOIN APP_RETAIL.ml_category c ON "
			+ "p.product_id = c.category_id WHERE c.category_name = ?1", countQuery = "SELECT COUNT(*) FROM APP_RETAIL.ml_product p", nativeQuery = true)
	Page<Product> findAllProductsByCategoryPaginated(
			@Param("category_name") String category, Pageable pageable);

}
