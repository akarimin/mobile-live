/*
 * Copyright (c) 2018 MobileLIVE corporation.
 *    This file is part of retail-service.
 *    retail-service is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *    retail-service is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with retail-service.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mobilelive.retail.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Profile;
import springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.*;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.time.temporal.Temporal;
import java.util.Collections;

import static ca.mobilelive.retail.constant.Constants.Profiles.DEVELOP;
import static com.google.common.base.Predicates.not;
import static springfox.documentation.builders.PathSelectors.any;

/**
 * @author akarimin@buffalo.edu
 */

@Profile(DEVELOP)
@EnableSwagger2
@Configuration
@Import(BeanValidatorPluginsConfiguration.class)
public class SwaggerConfig {

	@Bean
	public Docket api() {
		final ApiInfo apiInfo = new ApiInfo("MobileLIVE Code Challenge",
				"CRUD operations of products in a retail store", "1.0.0",
				"All rights reserved for Mobile-LIVE corp.",
				new Contact("Ali Karimi Nouri", "https://github.com/akarimin",
						"akarimin@buffalo.edu"),
				"LGPL-3.0", "https://gitlab.com/akarimin/mobile-live/blob/master/LICENSE",
				Collections.emptyList());
		return new Docket(DocumentationType.SWAGGER_2).useDefaultResponseMessages(false)
				.select().apis(RequestHandlerSelectors.any()).paths(any())
				.paths(not(PathSelectors.ant("/error")))
				.paths(not(PathSelectors.ant("/{uri}"))).build().apiInfo(apiInfo)
				.directModelSubstitute(Temporal.class, String.class);
	}

	@Bean
	public UiConfiguration uiConfig() {
		return UiConfigurationBuilder.builder().deepLinking(true)
				.displayOperationId(false).defaultModelsExpandDepth(1)
				.defaultModelExpandDepth(1).defaultModelRendering(ModelRendering.EXAMPLE)
				.displayRequestDuration(false).docExpansion(DocExpansion.NONE)
				.filter(false).maxDisplayedTags(null)
				.operationsSorter(OperationsSorter.ALPHA).showExtensions(false)
				.tagsSorter(TagsSorter.ALPHA)
				.supportedSubmitMethods(UiConfiguration.Constants.DEFAULT_SUBMIT_METHODS)
				.validatorUrl(null).build();
	}

}
