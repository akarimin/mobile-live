/*
 * Copyright (c) 2018 MobileLIVE corporation.
 *    This file is part of retail-service.
 *    retail-service is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *    retail-service is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with retail-service.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mobilelive.retail.i18;

import org.springframework.stereotype.Component;

import java.util.ListResourceBundle;

/**
 * @author akarimin
 */

@Component
public class Messages_fa extends ListResourceBundle {

	@Override
	protected Object[][] getContents() {
		return contents;
	}

	private Object[][] contents = {

			/*******************************************
			 * GENERAL
			 ******************************************************/
			{ "Exception.Exception.message",
					"خطای فنی. در صورت تکرار مشاهده با واحد پشتیبانی تماس بگیرید" },
			{ "validation.FieldMatchValidator.message",
					"هر دو فیلد باید مقدار یکسان داشه‌باشد" },
			/**********************************************************************************************************/
	};

}
