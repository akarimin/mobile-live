/*
 * Copyright (c) 2018 MobileLIVE corporation.
 *    This file is part of retail-service.
 *    retail-service is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *    retail-service is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with retail-service.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mobilelive.retail.i18;

import java.util.ListResourceBundle;

/**
 * @author akarimin
 */

public class Messages_us extends ListResourceBundle {

	@Override
	protected Object[][] getContents() {
		return contents;
	}

	private Object[][] contents = {

			/**********************************************
			 * GENERAL
			 ***************************************************/
			{ "Exception.Exception.message", "An Exception Occurred." },
			{ "validation.FieldMatchValidator.message", "Field validation violated." },
			{ "mobile.live.product.not.found.exception",
					"Product with input id does not exist in our database." },
			{ "mobile.live.code.is.blank.exception",
					"Product code input could not be blank." },
			{ "mobile.live.price.minimum.exception",
					"Product price could not be less than {value}." },
			{ "mobile.live.price.maximum.exception",
					"Product price could not be more than {value}." },
			{ "mobile.live.category.is.blank.exception",
					"Product category name could not be blank." },
			{ "controller.exception.handler.validator.exception",
					"Validation exception occurred." },
			/**********************************************************************************************************/
	};

}
