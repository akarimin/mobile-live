/*
 * Copyright (c) 2018 MobileLIVE corporation.
 *    This file is part of retail-service.
 *    retail-service is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *    retail-service is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with retail-service.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mobilelive.retail.exception.message;

/**
 * @author akarimin@buffalo.edu
 */

public class ProductNotFoundException extends Exception {

	private long productId;

	public ProductNotFoundException() {
	}

	public ProductNotFoundException(long productId) {
		this.productId = productId;
	}

	public ProductNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}

	public long getProductId() {
		return productId;
	}

	public ProductNotFoundException setProductId(long productId) {
		this.productId = productId;
		return this;
	}

}
