/*
 * Copyright (c) 2018 MobileLIVE corporation.
 *    This file is part of retail-service.
 *    retail-service is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *    retail-service is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with retail-service.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mobilelive.retail.exception.message;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author akarimin@buffalo.edu
 */

@ApiModel("Error")
public class ExceptionPayload {

	@ApiModelProperty(value = "Error code", example = "403")
	private String code;

	@ApiModelProperty(value = "Error message", example = "Login failed.")
	private String message;

	public ExceptionPayload() {
	}

	public ExceptionPayload(String code, String message) {
		this.code = code;
		this.message = message;
	}

	public String getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}

	public ExceptionPayload setCode(String code) {
		this.code = code;
		return this;
	}

	public ExceptionPayload setMessage(String message) {
		this.message = message;
		return this;
	}

	@Override
	public String toString() {
		return "ExceptionPayload{" + "code='" + code + '\'' + ", message='" + message
				+ '\'' + '}';
	}

}
