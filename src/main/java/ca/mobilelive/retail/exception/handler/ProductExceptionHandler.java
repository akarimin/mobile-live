/*
 * Copyright (c) 2018 MobileLIVE corporation.
 *    This file is part of retail-service.
 *    retail-service is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *    retail-service is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with retail-service.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mobilelive.retail.exception.handler;

import ca.mobilelive.retail.exception.message.ExceptionPayload;
import ca.mobilelive.retail.exception.message.ProductNotFoundException;
import ca.mobilelive.retail.exception.message.ResourceNotFoundException;
import ca.mobilelive.retail.util.Props;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import static org.springframework.http.HttpStatus.NOT_FOUND;

/**
 * @author akarimin@buffalo.edu
 */

@Component
@ControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
public class ProductExceptionHandler extends ResponseEntityExceptionHandler {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(ProductExceptionHandler.class);

	@ExceptionHandler({ ProductNotFoundException.class })
	protected ResponseEntity<ExceptionPayload> handleProductNotFoundException(
			ProductNotFoundException e) {
		ExceptionPayload responseMessage = new ExceptionPayload();
		LOGGER.error("mobile.live.product.not.found.exception id: {}", e.getProductId());
		responseMessage.setCode("PRODUCT_NOT_FOUND");
		responseMessage
				.setMessage(Props.property("mobile.live.product.not.found.exception"));
		return new ResponseEntity<>(responseMessage, NOT_FOUND);
	}

	@ExceptionHandler({ ResourceNotFoundException.class })
	protected ResponseEntity<ExceptionPayload> handleResourceNotFoundException(
			ResourceNotFoundException e) {
		ExceptionPayload responseMessage = new ExceptionPayload();
		LOGGER.error("mobile.live.resource.not.found.exception id: {}", e.getMessage());
		responseMessage.setCode("RESOURCE_NOT_FOUND");
		responseMessage
				.setMessage(Props.property("mobile.live.resource.not.found.exception"));
		return new ResponseEntity<>(responseMessage, NOT_FOUND);
	}

}
