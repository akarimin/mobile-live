/*
 * Copyright (c) 2018 MobileLIVE corporation.
 *    This file is part of retail-service.
 *    retail-service is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *    retail-service is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with retail-service.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mobilelive.retail.rest;

import ca.mobilelive.retail.dto.request.ProductDTO;
import ca.mobilelive.retail.dto.response.EmptyResponse;
import ca.mobilelive.retail.dto.response.GetProductListResponse;
import ca.mobilelive.retail.exception.message.ExceptionPayload;
import ca.mobilelive.retail.exception.message.ProductNotFoundException;
import ca.mobilelive.retail.jsr303.CheckBindingResult;
import ca.mobilelive.retail.service.ProductService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static ca.mobilelive.retail.dto.response.EmptyResponse.buildOrdinary;
import static org.springframework.http.ResponseEntity.ok;

/**
 * @author akarimin@buffalo.edu
 */

@Api(tags = "Product", description = "Providing CRUD operations endpoints on product entity in retail store.")
@RestController
@RequestMapping("/api/product")
public class ProductServiceRest {

	private final ProductService<ProductDTO> productService;

	@Autowired
	public ProductServiceRest(ProductService<ProductDTO> productService) {
		this.productService = productService;
	}

	@PostMapping
	@ApiOperation("Create a new product.")
	@ApiResponses({
			@ApiResponse(code = 200, message = "Create product succeeded.", response = EmptyResponse.class),
			@ApiResponse(code = 500, message = "Create product failed", response = ExceptionPayload.class) })
	@CheckBindingResult
	public ResponseEntity<ProductDTO> createProduct(
			@ApiParam("Request DTO for inserting product.") @Valid @RequestBody ProductDTO request,
			BindingResult bindingResult) {
		return ok(productService.createProduct(request));
	}

	@GetMapping("/{productId}")
	@ApiOperation("Retrieve a product by id. ")
	@ApiResponses({
			@ApiResponse(code = 200, message = "Retrieve product succeeded.", response = ProductDTO.class),
			@ApiResponse(code = 404, message = "Retrieve product failed", response = ExceptionPayload.class) })
	public ResponseEntity<ProductDTO> retrieveProduct(
			@ApiParam("ProductId by which the product is retrieved.") @PathVariable long productId)
			throws ProductNotFoundException {
		return ok(productService.retrieveProductById(productId));
	}

	@GetMapping("/list/{page}/{size}")
	@ApiOperation("Retrieve list of products.")
	@ApiResponses({
			@ApiResponse(code = 200, message = "Retrieve product list succeeded.", response = GetProductListResponse.class),
			@ApiResponse(code = 404, message = "Retrieve product list failed", response = ProductNotFoundException.class) })
	public ResponseEntity<GetProductListResponse> retrieveProductList(
			@ApiParam("Page number of product list.") @PathVariable int page,
			@ApiParam("Size of product list.") @PathVariable int size)
			throws ProductNotFoundException {
		return ok(productService.retrieveAllProduct(page, size));
	}

	@GetMapping("/list/{category}/{page}/{size}")
	@ApiOperation("Retrieve list of products by category.")
	@ApiResponses({
			@ApiResponse(code = 200, message = "Retrieve product list succeeded.", response = GetProductListResponse.class),
			@ApiResponse(code = 404, message = "Retrieve product list failed", response = ProductNotFoundException.class) })
	public ResponseEntity<GetProductListResponse> retrieveProductListByCategory(
			@ApiParam("Category of product.") @PathVariable String category,
			@ApiParam("Page number of product list.") @PathVariable int page,
			@ApiParam("Size of product list.") @PathVariable int size)
			throws ProductNotFoundException {
		return ok(productService.retrieveAllProductByCategory(category, page, size));
	}

	@PatchMapping
	@ApiOperation("Update a product.")
	@ApiResponses({
			@ApiResponse(code = 200, message = "Update product succeeded.", response = ProductDTO.class),
			@ApiResponse(code = 404, message = "Update product failed", response = ProductNotFoundException.class) })
	@CheckBindingResult
	public ResponseEntity<ProductDTO> updateProduct(
			@ApiParam("Updating product dto.") @Valid @RequestBody ProductDTO request,
			BindingResult bindingResult) throws ProductNotFoundException {
		return ok(productService.updateProduct(request));
	}

	@DeleteMapping("/{productId}")
	@ApiOperation("Delete a product.")
	@ApiResponses({
			@ApiResponse(code = 200, message = "Delete product succeeded.", response = EmptyResponse.class),
			@ApiResponse(code = 404, message = "Delete product failed", response = ProductNotFoundException.class) })
	public ResponseEntity<EmptyResponse> deleteProduct(
			@ApiParam("Deleting product id.") @PathVariable long productId)
			throws ProductNotFoundException {
		productService.deleteProductById(productId);
		return ok(buildOrdinary());
	}

}
