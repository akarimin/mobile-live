/*
 * Copyright (c) 2018 MobileLIVE corporation.
 *    This file is part of retail-service.
 *    retail-service is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *    retail-service is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with retail-service.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mobilelive.retail.mapper;

import ca.mobilelive.retail.domain.Product;
import ca.mobilelive.retail.dto.request.ProductDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

/**
 * @author akarimin@buffalo.edu
 */

@Mapper(componentModel = "spring", uses = CategoryMapper.class)
public interface ProductMapper extends EntityMapper<ProductDTO, Product> {

	@Override
	@Mappings({
			@Mapping(target = "category", expression = "java(entity.getCategory().getName())"),
			@Mapping(target = "productId", source = "id") })
	ProductDTO toDto(Product entity);

	@Override
	@Mapping(target = "id", source = "productId")
	Product toEntity(ProductDTO dto);

}
