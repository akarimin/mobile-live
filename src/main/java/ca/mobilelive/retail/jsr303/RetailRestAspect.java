/*
 * Copyright (c) 2018 MobileLIVE corporation.
 *    This file is part of retail-service.
 *    retail-service is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *    retail-service is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with retail-service.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mobilelive.retail.jsr303;

import ca.mobilelive.retail.exception.message.BeanValidationException;
import ca.mobilelive.retail.i18.I18Container;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;

/**
 * @author akarimin@buffalo.edu
 */

@Aspect
@Component
public class RetailRestAspect {

	@Pointcut("execution(* ca.mobilelive.retail.rest.*.*.*(..)) && @annotation(ca.mobilelive.retail.jsr303.CheckBindingResult) && args(..,bindingResult)")
	private void isAnnotatedWebService(BindingResult bindingResult) {
		System.out.println("AOP Called");
	}

	@Before("isAnnotatedWebService(bindingResult)")
	public void theAdvice(JoinPoint joinPoint, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			throw new BeanValidationException(I18Container.getBundle()
					.getObject("controller.exception.handler.validator.exception")
					.toString(), bindingResult);
		}
	}

}
