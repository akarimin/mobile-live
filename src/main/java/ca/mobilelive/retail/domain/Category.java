/*
 * Copyright (c) 2018 MobileLIVE corporation.
 *    This file is part of retail-service.
 *    retail-service is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *    retail-service is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with retail-service.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mobilelive.retail.domain;

import javax.persistence.*;
import java.util.Date;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * @author akarimin@buffalo.edu
 */

@Entity
@Table(name = "ml_category", schema = "APP.RETAIL")
public class Category {

	@Id
	@Column(name = "category_id")
	@GeneratedValue(strategy = IDENTITY)
	private Long id;

	@Column(name = "last_update")
	private Date lastUpdate;

	@Column(name = "category_name")
	private String name;

	public Long getId() {
		return id;
	}

	public Category setId(Long id) {
		this.id = id;
		return this;
	}

	public Date getLastUpdate() {
		return lastUpdate;
	}

	public Category setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
		return this;
	}

	public String getName() {
		return name;
	}

	public Category setName(String name) {
		this.name = name;
		return this;
	}

	@Override
	public String toString() {
		return "Category{" + "id=" + id + ", lastUpdate=" + lastUpdate + ", name='" + name
				+ '\'' + '}';
	}

}
