/*
 * Copyright (c) 2018 MobileLIVE corporation.
 *    This file is part of retail-service.
 *    retail-service is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *    retail-service is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with retail-service.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mobilelive.retail.domain;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author akarimin@buffalo.edu
 */

@Entity
@Table(name = "ml_product", schema = "APP.RETAIL")
public class Product {

	@Id
	@Column(name = "product_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "product_code")
	private String code;

	@Column(name = "description")
	private String description;

	@Column(name = "last_update")
	private Date lastUpdate;

	@Column(name = "price")
	private BigDecimal price;

	@Column(name = "quantity")
	private Integer quantity;

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private Category category;

	public Long getId() {
		return id;
	}

	public Product setId(Long id) {
		this.id = id;
		return this;
	}

	public String getCode() {
		return code;
	}

	public Product setCode(String code) {
		this.code = code;
		return this;
	}

	public String getDescription() {
		return description;
	}

	public Product setDescription(String description) {
		this.description = description;
		return this;
	}

	public Date getLastUpdate() {
		return lastUpdate;
	}

	public Product setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
		return this;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public Product setPrice(BigDecimal price) {
		this.price = price;
		return this;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public Product setQuantity(Integer quantity) {
		this.quantity = quantity;
		return this;
	}

	public Category getCategory() {
		return category;
	}

	public Product setCategory(Category category) {
		this.category = category;
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		Product product = (Product) o;

		if (id != null ? !id.equals(product.id) : product.id != null)
			return false;
		if (code != null ? !code.equals(product.code) : product.code != null)
			return false;
		if (description != null ? !description.equals(product.description)
				: product.description != null)
			return false;
		if (lastUpdate != null ? !lastUpdate.equals(product.lastUpdate)
				: product.lastUpdate != null)
			return false;
		if (price != null ? !price.equals(product.price) : product.price != null)
			return false;
		if (quantity != null ? !quantity.equals(product.quantity)
				: product.quantity != null)
			return false;
		return category != null ? category.equals(product.category)
				: product.category == null;
	}

	@Override
	public int hashCode() {
		int result = id != null ? id.hashCode() : 0;
		result = 31 * result + (code != null ? code.hashCode() : 0);
		result = 31 * result + (description != null ? description.hashCode() : 0);
		result = 31 * result + (lastUpdate != null ? lastUpdate.hashCode() : 0);
		result = 31 * result + (price != null ? price.hashCode() : 0);
		result = 31 * result + (quantity != null ? quantity.hashCode() : 0);
		result = 31 * result + (category != null ? category.hashCode() : 0);
		return result;
	}

	@Override
	public String toString() {
		return "Product{" + "id=" + id + ", code='" + code + '\'' + ", description='"
				+ description + '\'' + ", lastUpdate=" + lastUpdate + ", price=" + price
				+ ", quantity=" + quantity + ", category=" + category + '}';
	}

}
