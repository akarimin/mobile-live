/*
 * Copyright (c) 2018 MobileLIVE corporation.
 *    This file is part of retail-service.
 *    retail-service is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *    retail-service is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with retail-service.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mobilelive.retail.dto.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * @author akarimin@buffalo.edu
 */

@ApiModel("Product Category DTO")
public class CategoryDTO implements Serializable {

	@NotBlank(message = "mobile.live.category.is.blank.exception")
	@ApiModelProperty(value = "Product category name", example = "COSMETIC", required = true)
	private String name;

	public CategoryDTO() {
	}

	@JsonCreator
	public CategoryDTO(@NotBlank @JsonProperty("name") String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public CategoryDTO setName(String name) {
		this.name = name;
		return this;
	}

}
