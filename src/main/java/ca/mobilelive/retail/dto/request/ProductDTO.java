/*
 * Copyright (c) 2018 MobileLIVE corporation.
 *    This file is part of retail-service.
 *    retail-service is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *    retail-service is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with retail-service.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mobilelive.retail.dto.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author akarimin@buffalo.edu
 */

@ApiModel("Product Data Transfer Object.")
public class ProductDTO implements Serializable {

	@ApiModelProperty(value = "Product Id", example = "ONLY FILL IN CASE OF UPDATE")
	private Long productId;

	@NotBlank(message = "mobile.live.code.is.blank.exception")
	@ApiModelProperty(value = "Product Code", example = "B110", required = true, position = 1)
	private String code;

	@ApiModelProperty(value = "Product Description", example = "O'Real Pink Lipstick", position = 2)
	private String description;

	@ApiModelProperty(value = "Last time the product is updated", example = "2018-11-22T17:23:01.469Z", position = 3)
	private Date lastUpdate;

	@Min(value = 1, message = "mobile.live.price.minimum.exception")
	@Max(value = 999999999, message = "mobile.live.price.maximum.exception")
	@ApiModelProperty(value = "Product price in USD", example = "125", required = true, position = 4)
	private BigDecimal price;

	@ApiModelProperty(value = "Product quantity", example = "10", position = 5)
	private Integer quantity;

	@NotBlank(message = "mobile.live.category.is.blank.exception")
	@ApiModelProperty(value = "Product category", example = "COSMETIC", required = true, position = 6)
	private String category;

	public Long getProductId() {
		return productId;
	}

	public ProductDTO setProductId(Long productId) {
		this.productId = productId;
		return this;
	}

	public String getCode() {
		return code;
	}

	public ProductDTO setCode(String code) {
		this.code = code;
		return this;
	}

	public String getDescription() {
		return description;
	}

	public ProductDTO setDescription(String description) {
		this.description = description;
		return this;
	}

	public Date getLastUpdate() {
		return lastUpdate;
	}

	public ProductDTO setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
		return this;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public ProductDTO setPrice(BigDecimal price) {
		this.price = price;
		return this;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public ProductDTO setQuantity(Integer quantity) {
		this.quantity = quantity;
		return this;
	}

	public String getCategory() {
		return category;
	}

	public ProductDTO setCategory(String category) {
		this.category = category;
		return this;
	}

	@Override
	public String toString() {
		return "ProductDTO{" + "productId=" + productId + ", code='" + code + '\''
				+ ", description='" + description + '\'' + ", lastUpdate=" + lastUpdate
				+ ", price=" + price + ", quantity=" + quantity + ", category='"
				+ category + '\'' + '}';
	}

}
