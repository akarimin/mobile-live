/*
 * Copyright (c) 2018 MobileLIVE corporation.
 *    This file is part of retail-service.
 *    retail-service is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *    retail-service is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with retail-service.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mobilelive.retail.dto.response;

/**
 * @author akarimin@buffalo.edu
 */

public class EmptyResponse {

	private String message;

	private String responseType;

	public static EmptyResponse buildOrdinary() {
		return new EmptyResponse().setMessage("OPERATION SUCCEEDED.")
				.setResponseType("ORDINARY");
	}

	public EmptyResponse buildException() {
		return new EmptyResponse().setMessage("OPERATION FAILED.")
				.setResponseType("EXCEPTION");
	}

	public String getMessage() {
		return message;
	}

	public EmptyResponse setMessage(String message) {
		this.message = message;
		return this;
	}

	public String getResponseType() {
		return responseType;
	}

	public EmptyResponse setResponseType(String responseType) {
		this.responseType = responseType;
		return this;
	}

}
