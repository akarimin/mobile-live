/*
 * Copyright (c) 2018 MobileLIVE corporation.
 *    This file is part of retail-service.
 *    retail-service is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *    retail-service is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with retail-service.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mobilelive.retail.dto.response;

import ca.mobilelive.retail.dto.request.ProductDTO;

import java.util.List;

/**
 * @author akarimin@buffalo.edu
 */

public class GetProductListResponse {

	private List<ProductDTO> products;

	private long totalRecord;

	public List<ProductDTO> getProducts() {
		return products;
	}

	public GetProductListResponse setProducts(List<ProductDTO> products) {
		this.products = products;
		return this;
	}

	public long getTotalRecord() {
		return totalRecord;
	}

	public GetProductListResponse setTotalRecord(long totalRecord) {
		this.totalRecord = totalRecord;
		return this;
	}

	@Override
	public String toString() {
		return "GetProductListResponse{" + "products=" + products + ", totalRecord="
				+ totalRecord + '}';
	}

}
