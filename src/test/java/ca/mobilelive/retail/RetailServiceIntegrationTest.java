/*
 * Copyright (c) 2018 MobileLIVE corporation.
 *    This file is part of retail-service.
 *    retail-service is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *    retail-service is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with retail-service.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mobilelive.retail;

import ca.mobilelive.retail.dto.request.ProductDTO;
import ca.mobilelive.retail.dto.response.GetProductListResponse;
import ca.mobilelive.retail.exception.message.ExceptionPayload;
import org.assertj.core.api.Assertions;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.Date;

import static org.springframework.http.HttpHeaders.CONTENT_TYPE;
import static org.springframework.http.HttpMethod.DELETE;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.util.MimeTypeUtils.APPLICATION_JSON_VALUE;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@FixMethodOrder(MethodSorters.JVM)
public class RetailServiceIntegrationTest {

	@Autowired
	private TestRestTemplate restTemplate;

	@Test
	public void createProductRestTest() {
		ProductDTO productDTO = new ProductDTO().setCategory("TECHNOLOGY").setCode("T112")
				.setDescription("IPod Classic Apple 250Gb").setLastUpdate(new Date())
				.setPrice(new BigDecimal(120)).setQuantity(10);
		HttpHeaders headers = new HttpHeaders();
		headers.set(CONTENT_TYPE, APPLICATION_JSON_VALUE);
		HttpEntity<ProductDTO> httpEntity = new HttpEntity<>(productDTO, headers);
		ResponseEntity<ProductDTO> response = restTemplate.postForEntity("/api/product",
				httpEntity, ProductDTO.class);
		System.out.println("RESPONSE: " + response.getBody());
		Assertions.assertThat(response.getStatusCode()).isEqualTo(OK);
		Assertions.assertThat(response.getBody()).isNotNull();
		Assertions.assertThat(response.getBody().getCode()).isEqualTo("T112");
		Assertions.assertThat(response.getBody().getProductId()).isNotNull();
	}

	@Test
	public void getProductByIdRestTest() {
		ProductDTO productDTO = new ProductDTO().setCategory("TECHNOLOGY").setCode("T112")
				.setDescription("IPod Classic Apple 250Gb").setLastUpdate(new Date())
				.setPrice(new BigDecimal(120)).setQuantity(10);
		HttpHeaders headers = new HttpHeaders();
		headers.set(CONTENT_TYPE, APPLICATION_JSON_VALUE);
		HttpEntity<ProductDTO> httpEntity = new HttpEntity<>(productDTO, headers);
		ResponseEntity<ProductDTO> createResponse = restTemplate
				.postForEntity("/api/product", httpEntity, ProductDTO.class);
		System.out.println("RESPONSE: " + createResponse.getBody());
		Assertions.assertThat(createResponse.getStatusCode()).isEqualTo(OK);
		Assertions.assertThat(createResponse.getBody()).isNotNull();
		ResponseEntity<ProductDTO> response = restTemplate.getForEntity(
				"/api/product/" + createResponse.getBody().getProductId(),
				ProductDTO.class);
		System.out.println("RESPONSE: " + response.getBody());
		Assertions.assertThat(response.getStatusCode()).isEqualTo(OK);
		Assertions.assertThat(response.getBody()).isNotNull();
		Assertions.assertThat(response.getBody().getCode()).isEqualTo("T112");
		Assertions.assertThat(response.getBody().getProductId()).isNotNull();
	}

	@Test
	public void getProductByIdRestExceptionTest() {
		ResponseEntity<ExceptionPayload> response = restTemplate
				.getForEntity("/api/product/10", ExceptionPayload.class);
		System.out.println("RESPONSE: " + response.getBody());
		Assertions.assertThat(response.getStatusCode()).isEqualTo(NOT_FOUND);
		Assertions.assertThat(response.getBody()).isNotNull();
		Assertions.assertThat(response.getBody().getCode())
				.isEqualTo("PRODUCT_NOT_FOUND");
	}

	@Test
	public void getAllProductsPaginatedRestTest() {
		ResponseEntity<GetProductListResponse> response = restTemplate
				.getForEntity("/api/product/list/0/10", GetProductListResponse.class);
		System.out.println("RESPONSE: " + response.getBody());
		Assertions.assertThat(response.getStatusCode()).isEqualTo(OK);
		Assertions.assertThat(response.getBody()).isNotNull();
		Assertions.assertThat(response.getBody().getTotalRecord())
				.isEqualTo(response.getBody().getProducts().size());
		Assertions.assertThat(response.getBody().getProducts()).isNotNull();
	}

	@Test
	public void getAllProductsPaginatedRestEmptyTest() {
		ResponseEntity<GetProductListResponse> response = restTemplate
				.getForEntity("/api/product/list/1/10", GetProductListResponse.class);
		System.out.println("RESPONSE: " + response.getBody());
		Assertions.assertThat(response.getStatusCode()).isEqualTo(OK);
		Assertions.assertThat(response.getBody()).isNotNull();
		Assertions.assertThat(response.getBody().getTotalRecord())
				.isGreaterThanOrEqualTo(1);
		Assertions.assertThat(response.getBody().getProducts()).isEmpty();
	}

	@Test
	public void getAllProductsByCategoryPaginatedRestTest() {
		ResponseEntity<GetProductListResponse> response = restTemplate.getForEntity(
				"/api/product/list/TEHCNOLOGY/0/10", GetProductListResponse.class);
		System.out.println("RESPONSE: " + response.getBody());
		Assertions.assertThat(response.getStatusCode()).isEqualTo(OK);
		Assertions.assertThat(response.getBody()).isNotNull();
		Assertions.assertThat(response.getBody().getTotalRecord())
				.isEqualTo(response.getBody().getProducts().size());
		Assertions.assertThat(response.getBody().getProducts()).isNotNull();
	}

	@Test
	public void getAllProductsByCategoryPaginatedRestEmptyTest() {
		ResponseEntity<GetProductListResponse> response = restTemplate.getForEntity(
				"/api/product/list/TECHNOLOGY/1/10", GetProductListResponse.class);
		System.out.println("RESPONSE: " + response.getBody());
		Assertions.assertThat(response.getStatusCode()).isEqualTo(OK);
		Assertions.assertThat(response.getBody()).isNotNull();
		Assertions.assertThat(response.getBody().getTotalRecord())
				.isGreaterThanOrEqualTo(1);
		Assertions.assertThat(response.getBody().getProducts()).isEmpty();
	}

	@Test
	public void getAllProductsByCategoryPaginatedRestNotExistTest() {
		ResponseEntity<GetProductListResponse> response = restTemplate.getForEntity(
				"/api/product/list/COSMETIC/1/10", GetProductListResponse.class);
		System.out.println("RESPONSE: " + response.getBody());
		Assertions.assertThat(response.getStatusCode()).isEqualTo(OK);
		Assertions.assertThat(response.getBody()).isNotNull();
		Assertions.assertThat(response.getBody().getTotalRecord())
				.isGreaterThanOrEqualTo(1);
		Assertions.assertThat(response.getBody().getProducts()).isEmpty();
	}

	@Test
	public void updateProductRestTest() {
		ProductDTO productDTO = new ProductDTO().setProductId(1L).setCategory("HOME")
				.setCode("H112").setDescription("Leather Chair Double Brown")
				.setLastUpdate(new Date()).setPrice(new BigDecimal(1800)).setQuantity(3);
		HttpHeaders headers = new HttpHeaders();
		headers.set(CONTENT_TYPE, APPLICATION_JSON_VALUE);
		HttpEntity<ProductDTO> httpEntity = new HttpEntity<>(productDTO, headers);
		ResponseEntity<ProductDTO> response = restTemplate.postForEntity("/api/product",
				httpEntity, ProductDTO.class);
		System.out.println("RESPONSE: " + response.getBody());
		Assertions.assertThat(response.getStatusCode()).isEqualTo(OK);
		Assertions.assertThat(response.getBody()).isNotNull();
		Assertions.assertThat(response.getBody().getCode()).isEqualTo("H112");
		Assertions.assertThat(response.getBody().getProductId()).isNotNull();
	}

	@Test
	public void deleteProductRestTest() {
		restTemplate.delete("/api/product/1");
		ResponseEntity<ExceptionPayload> response = restTemplate
				.getForEntity("/api/product/1", ExceptionPayload.class);
		System.out.println("RESPONSE: " + response.getBody());
		Assertions.assertThat(response.getStatusCode()).isEqualTo(NOT_FOUND);
		Assertions.assertThat(response.getBody()).isNotNull();
		Assertions.assertThat(response.getBody().getCode())
				.isEqualTo("PRODUCT_NOT_FOUND");
	}

	@Test
	public void deleteProductRestExceptionTest() throws Exception {
		ResponseEntity<ExceptionPayload> exchange = restTemplate.exchange(
				"/api/product/10", DELETE, new HttpEntity<>(null),
				ExceptionPayload.class);
		Assertions.assertThat(exchange.getStatusCode()).isEqualTo(NOT_FOUND);
		Assertions.assertThat(exchange.getBody()).isNotNull();
		Assertions.assertThat(exchange.getBody().getCode())
				.isEqualTo("PRODUCT_NOT_FOUND");
	}

}
