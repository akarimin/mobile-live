/*
 * Copyright (c) 2018 MobileLIVE corporation.
 *    This file is part of retail-service.
 *    retail-service is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *    retail-service is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with retail-service.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mobilelive.retail;

import ca.mobilelive.retail.dto.request.ProductDTO;
import ca.mobilelive.retail.dto.response.GetProductListResponse;
import ca.mobilelive.retail.exception.message.ProductNotFoundException;
import ca.mobilelive.retail.service.ProductService;
import org.assertj.core.api.Assertions;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author akarimin@buffalo.edu
 */

@RunWith(SpringRunner.class)
@SpringBootTest
@FixMethodOrder(MethodSorters.JVM)
public class RetailServiceApplicationTest {

	@Autowired
	private ProductService<ProductDTO> productService;

	@Test(expected = InvalidDataAccessApiUsageException.class)
	public void whenProductDTOIsNull_thenCreateProductFailed() {
		productService.createProduct(null);
	}

	@Test
	public void whenProductDTOIsCorrect_thenCreateProductSucceeded() {
		ProductDTO dto = new ProductDTO().setCategory("TECHNOLOGY").setCode("F120")
				.setDescription("Samsung Galaxy Note 9").setLastUpdate(new Date())
				.setPrice(new BigDecimal(1200)).setQuantity(20);
		ProductDTO product = productService.createProduct(dto);
		Assertions.assertThat(product.getProductId()).isEqualTo(1);
		Assertions.assertThat(product).isNotNull();
	}

	@Test(expected = ProductNotFoundException.class)
	public void whenProductNotExist_thenRetrieveProductFailed()
			throws ProductNotFoundException {
		ProductDTO product = productService.retrieveProductById(2);
		Assertions.assertThat(product).isNull();
	}

	@Test
	public void whenProductExists_thenRetrieveProductSucceeded()
			throws ProductNotFoundException {
		ProductDTO dto = new ProductDTO().setCategory("TECHNOLOGY").setCode("F121")
				.setDescription("Samsung Galaxy Note 8").setLastUpdate(new Date())
				.setPrice(new BigDecimal(1000)).setQuantity(10);
		ProductDTO productDTO = productService.createProduct(dto);
		Assertions.assertThat(productDTO.getProductId()).isNotNull();
		ProductDTO product = productService
				.retrieveProductById(productDTO.getProductId());
		Assertions.assertThat(product).isNotNull();
	}

	@Test
	public void whenNotEnoughProductExist_thenRetrieveAllProductIsEmpty()
			throws ProductNotFoundException {
		GetProductListResponse list = productService.retrieveAllProduct(1, 10);
		Assertions.assertThat(list.getProducts().size()).isEqualTo(0);
		Assertions.assertThat(list.getTotalRecord()).isLessThan(10);
	}

	@Test
	public void whenEnoughProductExist_thenRetrieveAllProductsByCategorySucceeded()
			throws ProductNotFoundException {
		GetProductListResponse list = productService
				.retrieveAllProductByCategory("TECHNOLOGY", 0, 10);
		Assertions.assertThat(list.getProducts().size()).isNotEqualTo(0);
		Assertions.assertThat(list.getTotalRecord()).isLessThan(10);
	}

	@Test
	public void whenNotEnoughProductExist_thenRetrieveAllProductByCategoryIsEmpty()
			throws ProductNotFoundException {
		GetProductListResponse list = productService
				.retrieveAllProductByCategory("TECHNOLOGY", 1, 10);
		Assertions.assertThat(list.getProducts().size()).isEqualTo(0);
		Assertions.assertThat(list.getTotalRecord()).isLessThan(10);
	}

	@Test
	public void whenProductCategoryNotExist_thenRetrieveAllProductByCategoryIsEmpty()
			throws ProductNotFoundException {
		GetProductListResponse list = productService
				.retrieveAllProductByCategory("COSMETIC", 0, 10);
		Assertions.assertThat(list.getProducts().size()).isEqualTo(0);
		Assertions.assertThat(list.getTotalRecord()).isLessThan(10);
	}

	@Test
	public void whenEnoughProductExist_thenRetrieveAllProductSucceeded()
			throws ProductNotFoundException {
		GetProductListResponse list = productService.retrieveAllProduct(0, 10);
		Assertions.assertThat(list.getProducts().size()).isNotEqualTo(0);
		Assertions.assertThat(list.getTotalRecord()).isLessThan(10);
	}

	@Test(expected = ProductNotFoundException.class)
	public void whenProductNotExists_thenUpdateProductFailed()
			throws ProductNotFoundException {
		ProductDTO dto = new ProductDTO().setProductId(3L).setCategory("TECHNOLOGY")
				.setCode("F122").setDescription("Samsung Galaxy S9")
				.setLastUpdate(new Date()).setPrice(new BigDecimal(800)).setQuantity(13);
		ProductDTO productDTO = productService.updateProduct(dto);
		Assertions.assertThat(productDTO).isNull();
	}

	@Test
	public void whenProductExists_thenUpdateProductSucceeded()
			throws ProductNotFoundException {
		ProductDTO dto = new ProductDTO().setProductId(1L).setCategory("TECHNOLOGY")
				.setCode("F122").setDescription("Samsung Galaxy S9")
				.setLastUpdate(new Date()).setPrice(new BigDecimal(800)).setQuantity(13);
		ProductDTO productDTO = productService.updateProduct(dto);
		Assertions.assertThat(productDTO.getProductId()).isEqualTo(dto.getProductId());
		Assertions.assertThat(productDTO).isNotNull();
	}

	@Test(expected = ProductNotFoundException.class)
	public void whenProductNotExists_thenDeleteProductFailed()
			throws ProductNotFoundException {
		productService.deleteProductById(4);
	}

	@Test
	public void whenProductExists_thenDeleteProductSucceeded()
			throws ProductNotFoundException {
		GetProductListResponse listBefore = productService.retrieveAllProduct(0, 10);
		productService.deleteProductById(1);
		GetProductListResponse listAfter = productService.retrieveAllProduct(0, 10);
		Assertions.assertThat(
				listAfter.getProducts().stream().anyMatch(x -> x.getProductId() == 1))
				.isFalse();
		Assertions.assertThat(listBefore.getTotalRecord() - listAfter.getTotalRecord())
				.isEqualTo(1);
	}

}
